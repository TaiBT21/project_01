"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.port = exports.database = void 0;

var _dotenv = require("dotenv");

(0, _dotenv.config)();
var database = {
  connectionLimit: 10,
  host: process.env.DATABASE_HOST || "localhost",
  user: process.env.DATABASE_USER || "root",
  password: process.env.DATABASE_PASSWORD || "mypassword",
  database: process.env.DATABASE_NAME || "dblinks"
};
exports.database = database;
var port = process.env.PORT || 4000;
exports.port = port;